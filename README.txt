
	This module is an XML aggregator. Currently it supports RSS 2.0 but it is designed to aggregate from any XML format by adding feed_handlers... More on that below...

1. Requirements :

	This module REQUIRES PHP 5.0 or later and CURL support to function properly. Furthermore, if you plan to aggregate
articles with images, then the IMAGE module is requiered as well.

2. Installation :

	Simply enable the module and you're set to go. Upon enabling this module, you'll have access to 2 new node types, Feed and Feed Item. A vocabulary will also be created called "Aggregation Feed Types"

3. Configuration :

	You can configure the module from admin -> settings.

4. Use :

	For every feed class in the system (i.e. ATOM, RSS, MY_CUSTOM_FEED_TYPE, etc), a term needs to be created under the vocabulary "Aggregation Feed Types". This term, when assigned to a feed, will cause a file corresponding to the term name to be called (ex. RSS20.inc), the file should reside in the feed_handlers directory. See example.inc for detailed API and RSS20.inc for an example.

5. Notes :

IMPORTANT!!!

	If you are working on a handler for a public XML feed type (ex. RSS, ATOM, NEWSML, etc) that you would like to contribute to this project, please let me know. This is necessary to avoid everyone re-working the same feed types over and over. The contributed feed_handler will be tested and incorporated into the next release later on.

	If you would like to add a feed handler for a custom XML format that you think will be beneficial to contribute, then please provide a sample XML file and the handler file. Your format may become widely used because a handler is available :-)

6. contact Information

	Please contribute your suggestions, bugs and issues at the project's page http://drupal.org/project/aggregation

	For professional drupal consultation contact me at aamayreh@gmail.com