<?php

/**
 * This is RSS 2.0 parsing callback function
 */
function _aggregation_RSS20_parse($feed_XML, $feed)
{
	foreach ($feed_XML->xpath('//item') AS $news)
	{
		if ($news->guid)
			$guid = (string)$news->guid;
		else
			$guid = NULL;
			
		$title = (string)$news->title;
		$body = (string)$news->description;
		$teaser = node_teaser($body);
		$original_author = $feed->original_author;
		
		if ($news->link)
			$original_url = (string)$news->link;
		else 
			$original_url = NULL;
			
		$timestamp = strtotime((string)$news->pubDate);
		if ($timestamp === FALSE)
			$timestamp = time();
			
		_aggregation_add_item($title, $body, $teaser, $original_author, $feed, array(), $timestamp, $original_url, $guid, array());
	}
}